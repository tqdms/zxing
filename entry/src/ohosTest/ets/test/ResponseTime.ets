/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import { AztecDecoder, AztecDetectorResult,
  BitArray,
  BitMatrix,
  DataMatrixDecodedBitStreamParser,
  EncodeHintType,
  LuminanceSource,
  MathUtils,
  PDF417DecodedBitStreamParser,
  PDF417ResultMetadata,
  PlanarYUVLuminanceSource,
  QRCodeByteMatrix,
  QRCodeDecoderErrorCorrectionLevel,
  QRCodeEncoder,
  QRCodeEncoderQRCode,
  QRCodeMode,
  QRCodeVersion,
  ResultPoint,
  RGBLuminanceSource
} from '@ohos/zxing';

const BASE_COUNT: number = 2000;
export default function ResponseTime() {
  const EPSILON: number /*float*/ = 1.0E-8;
  const YUV: Uint8ClampedArray = Uint8ClampedArray.from([
    0, 1, 1, 2, 3, 5,
    8, 13, 21, 34, 55, 89,
    0, -1, -1, -2, -3, -5,
    -8, -13, -21, -34, -55, -89,
    127, 127, 127, 127, 127, 127,
    127, 127, 127, 127, 127, 127,
  ]);

  const COLS: number /*int*/ = 6;
  const ROWS: number /*int*/ = 4;
  const Y = new Uint8ClampedArray(COLS * ROWS);
  const SOURCE = new RGBLuminanceSource(Int32Array.from([
    0x000000, 0x7F7F7F, 0xFFFFFF,
    0xFF0000, 0x00FF00, 0x0000FF,
    0x0000FF, 0x00FF00, 0xFF0000]), 3, 3);
  describe('ResponseTime', () => {
    it('testAsciiStandardDecode.Time',0, () => {
      // ASCII characters 0-127 are encoded as the value + 1
      const bytes: Uint8Array = new Uint8Array(6);
      bytes[0] = 'a'.charCodeAt(0) + 1;
      bytes[1] = 'b'.charCodeAt(0) + 1;
      bytes[2] = 'c'.charCodeAt(0) + 1;
      bytes[3] = 'A'.charCodeAt(0) + 1;
      bytes[4] = 'B'.charCodeAt(0) + 1;
      bytes[5] = 'C'.charCodeAt(0) + 1;
      const decodedString = DataMatrixDecodedBitStreamParser.decode(bytes).getText();
      expect(decodedString).assertEqual('abcABC')
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        DataMatrixDecodedBitStreamParser.decode(bytes).getText();
      }
      endTime(startTime, 'testAsciiStandardDecode');
    })
    it('assertContain.Time',0, () => {
      const bytes: Uint8Array = new Uint8Array(4);
      bytes[0] = 130;
      bytes[1] = 1 + 130;
      bytes[2] = 98 + 130;
      bytes[3] = 99 + 130;
      const decodedString = DataMatrixDecodedBitStreamParser.decode(bytes).getText();
      expect(decodedString).assertEqual('00019899')
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        DataMatrixDecodedBitStreamParser.decode(bytes).getText();
      }
      endTime(startTime, 'testAsciiStandardDecode');
    })
    it('testAztecResult.Time',0, () => {
      const matrix = BitMatrix.parseFromString(
        'X X X X X     X X X       X X X     X X X     \n' +
          'X X X     X X X     X X X X     X X X     X X \n' +
          '  X   X X       X   X   X X X X     X     X X \n' +
          '  X   X X     X X     X     X   X       X   X \n' +
          '  X X   X X         X               X X     X \n' +
          '  X X   X X X X X X X X X X X X X X X     X   \n' +
          '  X X X X X                       X   X X X   \n' +
          '  X   X   X   X X X X X X X X X   X X X   X X \n' +
          '  X   X X X   X               X   X X       X \n' +
          '  X X   X X   X   X X X X X   X   X X X X   X \n' +
          '  X X   X X   X   X       X   X   X   X X X   \n' +
          '  X   X   X   X   X   X   X   X   X   X   X   \n' +
          '  X X X   X   X   X       X   X   X X   X X   \n' +
          '  X X X X X   X   X X X X X   X   X X X   X X \n' +
          'X X   X X X   X               X   X   X X   X \n' +
          '  X       X   X X X X X X X X X   X   X     X \n' +
          '  X X   X X                       X X   X X   \n' +
          '  X X X   X X X X X X X X X X X X X X   X X   \n' +
          'X     X     X     X X   X X               X X \n' +
          'X   X X X X X   X X X X X     X   X   X     X \n' +
          'X X X   X X X X           X X X       X     X \n' +
          'X X     X X X     X X X X     X X X     X X   \n' +
          '    X X X     X X X       X X X     X X X X   \n',
        'X ', '  ');

      const NO_POINTS: ResultPoint[] = [];
      const r = new AztecDetectorResult(matrix, NO_POINTS, false, 30, 2);
      const result = new AztecDecoder().decode(r);
      expect('88888TTTTTTTTTTTTTTTTTTTTTTTTTTTTTT').assertEqual(result.getText())
      expect(180).assertEqual(result.getNumBits())
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        new AztecDecoder().decode(r);
      }
      endTime(startTime, 'testAsciiStandardDecode');
    })
    it('testEncode.Time',0, () => {
      const qrCode: QRCodeEncoderQRCode = QRCodeEncoder.encode('ABCDEF', QRCodeDecoderErrorCorrectionLevel.H);
      const expected: string =
        '<<\n' +
          ' mode: ALPHANUMERIC\n' +
          ' ecLevel: H\n' +
          ' version: 1\n' +
          ' maskPattern: 4\n' +
          ' matrix:\n' +
          ' 1 1 1 1 1 1 1 0 0 1 0 1 0 0 1 1 1 1 1 1 1\n' +
          ' 1 0 0 0 0 0 1 0 1 0 1 0 1 0 1 0 0 0 0 0 1\n' +
          ' 1 0 1 1 1 0 1 0 0 0 0 0 0 0 1 0 1 1 1 0 1\n' +
          ' 1 0 1 1 1 0 1 0 0 1 0 0 1 0 1 0 1 1 1 0 1\n' +
          ' 1 0 1 1 1 0 1 0 0 1 0 1 0 0 1 0 1 1 1 0 1\n' +
          ' 1 0 0 0 0 0 1 0 1 0 0 1 1 0 1 0 0 0 0 0 1\n' +
          ' 1 1 1 1 1 1 1 0 1 0 1 0 1 0 1 1 1 1 1 1 1\n' +
          ' 0 0 0 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 0 0 0\n' +
          ' 0 0 0 0 1 1 1 1 0 1 1 0 1 0 1 1 0 0 0 1 0\n' +
          ' 0 0 0 0 1 1 0 1 1 1 0 0 1 1 1 1 0 1 1 0 1\n' +
          ' 1 0 0 0 0 1 1 0 0 1 0 1 0 0 0 1 1 1 0 1 1\n' +
          ' 1 0 0 1 1 1 0 0 1 1 1 1 0 0 0 0 1 0 0 0 0\n' +
          ' 0 1 1 1 1 1 1 0 1 0 1 0 1 1 1 0 0 1 1 0 0\n' +
          ' 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1 0 0 0 1 0 1\n' +
          ' 1 1 1 1 1 1 1 0 1 1 1 1 0 0 0 0 0 1 1 0 0\n' +
          ' 1 0 0 0 0 0 1 0 1 1 0 1 0 0 0 1 0 1 1 1 1\n' +
          ' 1 0 1 1 1 0 1 0 1 0 0 1 0 0 0 1 1 0 0 1 1\n' +
          ' 1 0 1 1 1 0 1 0 0 0 1 1 0 1 0 0 0 0 1 1 1\n' +
          ' 1 0 1 1 1 0 1 0 0 1 0 1 0 0 0 1 1 0 0 0 0\n' +
          ' 1 0 0 0 0 0 1 0 0 1 0 0 1 0 0 1 1 0 0 0 1\n' +
          ' 1 1 1 1 1 1 1 0 0 0 1 0 0 1 0 0 0 0 1 1 1\n' +
          '>>\n';

      expect(qrCode.toString()).assertEqual(expected)
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        QRCodeEncoder.encode('testEncode', QRCodeDecoderErrorCorrectionLevel.H);
      }
      endTime(startTime, 'testAsciiStandardDecode');
    })
    it('testSimpleUTF8ECI.Time',0, () => {
      const hints:Map<EncodeHintType, string> = new Map<EncodeHintType, string>(); // EncodeHintType.class)
      hints.set(EncodeHintType.CHARACTER_SET, 'UTF8');
      const qrCode: QRCodeEncoderQRCode = QRCodeEncoder.encode('hello', QRCodeDecoderErrorCorrectionLevel.H, hints);
      const expected: string =
        '<<\n' +
          ' mode: BYTE\n' +
          ' ecLevel: H\n' +
          ' version: 1\n' +
          ' maskPattern: 6\n' +
          ' matrix:\n' +
          ' 1 1 1 1 1 1 1 0 0 0 1 1 0 0 1 1 1 1 1 1 1\n' +
          ' 1 0 0 0 0 0 1 0 0 0 1 1 0 0 1 0 0 0 0 0 1\n' +
          ' 1 0 1 1 1 0 1 0 1 0 0 1 1 0 1 0 1 1 1 0 1\n' +
          ' 1 0 1 1 1 0 1 0 1 0 0 0 1 0 1 0 1 1 1 0 1\n' +
          ' 1 0 1 1 1 0 1 0 0 1 1 0 0 0 1 0 1 1 1 0 1\n' +
          ' 1 0 0 0 0 0 1 0 0 0 0 1 0 0 1 0 0 0 0 0 1\n' +
          ' 1 1 1 1 1 1 1 0 1 0 1 0 1 0 1 1 1 1 1 1 1\n' +
          ' 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0 0 0 0 0 0\n' +
          ' 0 0 0 1 1 0 1 1 0 0 0 0 1 0 0 0 0 1 1 0 0\n' +
          ' 0 0 0 0 0 0 0 0 1 1 0 1 0 0 1 0 1 1 1 1 1\n' +
          ' 1 1 0 0 0 1 1 1 0 0 0 1 1 0 0 1 0 1 0 1 1\n' +
          ' 0 0 0 0 1 1 0 0 1 0 0 0 0 0 1 0 1 1 0 0 0\n' +
          ' 0 1 1 0 0 1 1 0 0 1 1 1 0 1 1 1 1 1 1 1 1\n' +
          ' 0 0 0 0 0 0 0 0 1 1 1 0 1 1 1 1 1 1 1 1 1\n' +
          ' 1 1 1 1 1 1 1 0 1 0 1 0 0 0 1 0 0 0 0 0 0\n' +
          ' 1 0 0 0 0 0 1 0 0 1 0 0 0 1 0 0 0 1 1 0 0\n' +
          ' 1 0 1 1 1 0 1 0 1 0 0 0 1 0 1 0 0 0 1 0 0\n' +
          ' 1 0 1 1 1 0 1 0 1 1 1 1 0 1 0 0 1 0 1 1 0\n' +
          ' 1 0 1 1 1 0 1 0 0 1 1 1 0 0 1 0 0 1 0 1 1\n' +
          ' 1 0 0 0 0 0 1 0 0 0 0 0 0 1 1 0 1 1 0 0 0\n' +
          ' 1 1 1 1 1 1 1 0 0 0 0 1 0 1 0 0 1 0 1 0 0\n' +
          '>>\n';

      expect(qrCode.toString()).assertEqual(expected)
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        QRCodeEncoder.encode('hello', QRCodeDecoderErrorCorrectionLevel.H, hints);
      }
      endTime(startTime, 'testSimpleUTF8ECI');
    })
    it('testAppendModeInfo.Time',0, () => {
      const bits = new BitArray();
      QRCodeEncoder.appendModeInfo(QRCodeMode.NUMERIC, bits);
      expect(bits.toString()).assertEqual(' ...X')
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        QRCodeEncoder.appendModeInfo(QRCodeMode.NUMERIC, bits);
      }
      endTime(startTime, 'testAppendModeInfo');
    })
    it('testEncodeWithVersion.Time',0, () => {

      const hints:Map<EncodeHintType, number> = new Map<EncodeHintType, number>(); // EncodeHintType.class)
      hints.set(EncodeHintType.QR_VERSION, 7);
      const qrCode: QRCodeEncoderQRCode = QRCodeEncoder.encode('ABCDEF', QRCodeDecoderErrorCorrectionLevel.H, hints);

      expect(qrCode.toString().indexOf(' version: 7\n') !== -1).assertTrue()
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        QRCodeEncoder.encode('ABCDEF', QRCodeDecoderErrorCorrectionLevel.H, hints);
      }
      endTime(startTime, 'testEncodeWithVersion');
    })
    it('testRound.Time',0, () => {

      expect(MathUtils.round(-1.0)).assertEqual(-1)
      expect(MathUtils.round(0.0)).assertEqual(0)
      expect(MathUtils.round(1.0)).assertEqual(1)

      expect(MathUtils.round(1.9)).assertEqual(2)
      expect(MathUtils.round(2.1)).assertEqual(2)

      expect(MathUtils.round(2.5)).assertEqual(3)

      expect(MathUtils.round(-1.9)).assertEqual(-2)
      expect(MathUtils.round(-2.1)).assertEqual(-2)

      expect(MathUtils.round(-2.5)).assertEqual(-3)

      expect(MathUtils.round(Number.MAX_SAFE_INTEGER)).assertEqual(Number.MAX_SAFE_INTEGER)
      expect(MathUtils.round(Number.MIN_SAFE_INTEGER)).assertEqual(Number.MIN_SAFE_INTEGER)

      expect(MathUtils.round(Number.POSITIVE_INFINITY)).assertEqual(Number.MAX_SAFE_INTEGER)
      expect(MathUtils.round(Number.NEGATIVE_INFINITY)).assertEqual(Number.MIN_SAFE_INTEGER)
      // expect(MathUtils.round(NaN)).assertEqual(0)
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        MathUtils.round(Number.MIN_SAFE_INTEGER);
      }
      endTime(startTime, 'testAsciiStandardDecode');
    })
    it('testDistance.Time',0, () => {
      expect(Math.abs(MathUtils.distance(1.0, 2.0, 3.0, 4.0) - /*(float) */Math.sqrt(8.0)) < EPSILON).assertTrue()
      expect(Math.abs(MathUtils.distance(1.0, 2.0, 1.0, 2.0) - 0.0) < EPSILON).assertTrue()
      expect(Math.abs(MathUtils.distance(1, 2, 3, 4) - /*(float) */Math.sqrt(8.0)) < EPSILON).assertTrue()
      expect(Math.abs(MathUtils.distance(1, 2, 1, 2) - 0.0) < EPSILON).assertTrue()
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        MathUtils.distance(1.0, 2.0, 3.0, 4.0);
      }
      endTime(startTime, 'testDistance');
    })
    it('testSum.Time',0, () => {

      expect(MathUtils.sum(Int32Array.from([]))).assertEqual(0)
      expect(MathUtils.sum(Int32Array.from([1]))).assertEqual(1)
      expect(MathUtils.sum(Int32Array.from([1, 3]))).assertEqual(4)
      expect(MathUtils.sum(Int32Array.from([-1, 1]))).assertEqual(0)
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        MathUtils.sum(Int32Array.from([]));
      }
      endTime(startTime, 'testSum');
    })
    it('testStandardSample2.Time',0, () => {
      const resultMetadata = new PDF417ResultMetadata();
      const sampleCodes = Int32Array.from([11, 928, 111, 103, 17, 53, 923, 1, 111, 104, 922,
        // we should never reach these
        1000, 1000, 1000]);

      PDF417DecodedBitStreamParser.decodeMacroBlock(sampleCodes, 2, resultMetadata);

      expect(3).assertEqual(resultMetadata.getSegmentIndex())
      expect('ARBX').assertEqual(resultMetadata.getFileId())
      expect(resultMetadata.isLastSegment()).assertTrue()
      expect(4).assertEqual(resultMetadata.getSegmentCount())
      expect(resultMetadata.getAddressee()).assertNull()
      expect(resultMetadata.getSender()).assertNull()
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        PDF417DecodedBitStreamParser.decodeMacroBlock(sampleCodes, 2, resultMetadata);
      }
      endTime(startTime, 'testAsciiStandardDecode');

    })
    it('testNoCrop.Time',0, () => {
      const source = new PlanarYUVLuminanceSource(YUV, COLS, ROWS, 0, 0, COLS, ROWS, false);
      assertTypedArrayEquals(Y, 0, source.getMatrix(), 0, Y.length);
      for (let r: number /*int*/ = 0; r < ROWS; r++) {
        assertTypedArrayEquals(Y, r * COLS, source.getRow(r), 0, COLS);
      }
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        assertTypedArrayEquals(Y, 0, source.getMatrix(), 0, Y.length);
      }
      endTime(startTime, 'testNoCrop');
    })
    it('testCrop.Time',0, () => {
      const source =
        new PlanarYUVLuminanceSource(YUV, COLS, ROWS, 1, 1, COLS - 2, ROWS - 2, false);
      expect(source.isCropSupported()).assertTrue()
      const cropMatrix: Uint8ClampedArray = source.getMatrix();
      for (let r: number /*int*/ = 0; r < ROWS - 2; r++) {
        assertTypedArrayEquals(Y, (r + 1) * COLS + 1, cropMatrix, r * (COLS - 2), COLS - 2);
      }
      for (let r: number /*int*/ = 0; r < ROWS - 2; r++) {
        assertTypedArrayEquals(Y, (r + 1) * COLS + 1, source.getRow(r), 0, COLS - 2);
      }
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        source.isCropSupported();
      }
      endTime(startTime, 'testCrop');
    })
    it('test.Time',0, () => {
      const qrCode = new QRCodeEncoderQRCode();

      // First, test simple setters and getters.
      // We use numbers of version 7-H.
      qrCode.setMode(QRCodeMode.BYTE);
      qrCode.setECLevel(QRCodeDecoderErrorCorrectionLevel.H);
      qrCode.setVersion(QRCodeVersion.getVersionForNumber(7));
      qrCode.setMaskPattern(3);

      expect(QRCodeMode.BYTE.equals(qrCode.getMode())).assertTrue()
      expect(QRCodeDecoderErrorCorrectionLevel.H.equals(qrCode.getECLevel())).assertTrue()
      expect(qrCode.getVersion().getVersionNumber()).assertEqual(7)
      expect(qrCode.getMaskPattern()).assertEqual(3)

      // Prepare the matrix.
      const matrix = new QRCodeByteMatrix(45, 45);
      // Just set bogus zero/one values.
      for (let y: number /*int*/ = 0; y < 45; ++y) {
        for (let x: number /*int*/ = 0; x < 45; ++x) {
          matrix.setNumber(x, y, (y + x) % 2);
        }
      }

      // Set the matrix.
      qrCode.setMatrix(matrix);
      expect(matrix.equals(qrCode.getMatrix())).assertTrue()

      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        qrCode.setMode(QRCodeMode.BYTE);
      }
      endTime(startTime, 'setMode');

      let startTime1 = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        qrCode.setECLevel(QRCodeDecoderErrorCorrectionLevel.H);
      }
      endTime(startTime1, 'setECLevel');
      let startTime2 = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        qrCode.setVersion(QRCodeVersion.getVersionForNumber(7));
      }
      endTime(startTime2, 'setVersion');
      let startTime3 = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        qrCode.setMaskPattern(3);
      }
      endTime(startTime3, 'setVersion');
    })
    it('testToString1.Time',0, () => {
      const qrCode = new QRCodeEncoderQRCode();
      const expected: string =
        '<<\n' +
          ' mode: null\n' +
          ' ecLevel: null\n' +
          ' version: null\n' +
          ' maskPattern: -1\n' +
          ' matrix: null\n' +
          '>>\n';
      expect(qrCode.toString()).assertEqual(expected)

      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        qrCode.toString();
      }
      endTime(startTime, 'testToString1');
    })
    it('testToString2.Time',0, () => {
      const qrCode = new QRCodeEncoderQRCode();
      qrCode.setMode(QRCodeMode.BYTE);
      qrCode.setECLevel(QRCodeDecoderErrorCorrectionLevel.H);
      qrCode.setVersion(QRCodeVersion.getVersionForNumber(1));
      qrCode.setMaskPattern(3);
      const matrix = new QRCodeByteMatrix(21, 21);
      for (let y: number /*int*/ = 0; y < 21; ++y) {
        for (let x: number /*int*/ = 0; x < 21; ++x) {
          matrix.setNumber(x, y, (y + x) % 2);
        }
      }
      qrCode.setMatrix(matrix);
      const expected: string = '<<\n' +
        ' mode: BYTE\n' +
        ' ecLevel: H\n' +
        ' version: 1\n' +
        ' maskPattern: 3\n' +
        ' matrix:\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        ' 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1\n' +
        ' 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0\n' +
        '>>\n';

      expect(qrCode.toString()).assertEqual(expected)

      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        qrCode.toString();
      }
      endTime(startTime, 'testToString2');
    })
    it('testCrop.Time',0, () => {

      expect(SOURCE.isCropSupported()).assertTrue()
      const cropped: LuminanceSource = SOURCE.crop(1, 1, 1, 1);
      expect(cropped.getHeight()).assertEqual(1)
      expect(cropped.getWidth()).assertEqual(1)
      expect(typedArraysAreEqual(Uint8ClampedArray.from([0x7F]), cropped.getRow(0))).assertTrue()
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        SOURCE.isCropSupported();
      }
      endTime(startTime, 'testCrop');
    })
    it('testMatrix.Time',0, () => {

      expect(typedArraysAreEqual(Uint8ClampedArray.from([0x00, 0x7F, 0xFF, 0x3F, 0x7F, 0x3F, 0x3F, 0x7F, 0x3F]),
        SOURCE.getMatrix())).assertTrue()
      const croppedFullWidth: LuminanceSource = SOURCE.crop(0, 1, 3, 2);
      expect(typedArraysAreEqual(Uint8ClampedArray.from([0x3F, 0x7F, 0x3F, 0x3F, 0x7F, 0x3F]),
        croppedFullWidth.getMatrix())).assertTrue()
      const croppedCorner: LuminanceSource = SOURCE.crop(1, 1, 2, 2);

      expect(typedArraysAreEqual(Uint8ClampedArray.from([0x7F, 0x3F, 0x7F, 0x3F]),
        croppedCorner.getMatrix())).assertTrue()

      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        SOURCE.getMatrix();
      }
      endTime(startTime, 'testMatrix');
    })
    it('testGetRow.Time',0, () => {
      expect(typedArraysAreEqual(Uint8ClampedArray.from([0x3F, 0x7F, 0x3F]), SOURCE.getRow(2, new Uint8ClampedArray(3)))).assertTrue()
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        SOURCE.getRow(2, new Uint8ClampedArray(3));
      }
      endTime(startTime, 'testGetRow');
    })
    it('testToString.Time',0, () => {
      expect(SOURCE.toString()).assertEqual('#+ \n#+#\n#+#\n')
      let startTime = new Date().getTime()
      for (let index = 0; index < BASE_COUNT; index++) {
        SOURCE.toString();
      }
      endTime(startTime, 'testToString');
    })
  })
}
function typedArraysAreEqual(left: Int32Array | Uint8ClampedArray, right: Int32Array | Uint8ClampedArray, size?: number): boolean {
  if (!size) {
    size = Math.max(left.length, right.length);
  }

  for (let i = 0; i < size; i++) {
    if (left[i] !== right[i]) {
      return false;
    }
  }

  return true;
}
function assertTypedArrayEquals(expected: Uint8ClampedArray, expectedFrom: number /*int*/,
                                actual: Uint8ClampedArray, actualFrom: number /*int*/,
                                length: number /*int*/) {
  for (let i: number /*int*/ = 0; i < length; i++) {
    expect(actual[actualFrom + i]).assertEqual(expected[expectedFrom + i])
  }
}
function endTime(startTime: number, tag: string) {
  console.log(tag + ":startTime:" + startTime)
  let endTime: number = new Date().getTime();
  let averageTime = ((endTime - startTime) * 1000 / BASE_COUNT)
  console.log(tag + ":endTime:" + endTime)
  console.log(tag + ":averageTime:" + averageTime + "μs");
}