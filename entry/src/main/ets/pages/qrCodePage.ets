/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BarcodeFormat } from '@ohos/zxing';
import QRCode from '../util/QRCode'
import image from '@ohos.multimedia.image'

@Entry
@Component
struct QrCodePage {
  @State pixelMap: image.PixelMap | undefined = undefined
  @State message: string = ''
  @State inputText: string = ''
  qrcode = new QRCode()

  async encode() {
    this.pixelMap = await this.qrcode.encode(this.inputText, {
      width: 260,
      height: 260,
      format: BarcodeFormat.QR_CODE
    })
  }

  async decode() {
    try {
      this.message = await this.qrcode.decode(this.pixelMap as image.PixelMap , {
        width: 260,
        height: 260,
        format: BarcodeFormat.QR_CODE
      })
    } catch (err) {

      console.log('[Demo] decode error:' + JSON.stringify(err));
    }
  }

  build() {

    Column() {
      if(this.pixelMap) {
        Image(this.pixelMap).width(260).height(260).margin(30)
      }
      Text('解析结果：' + this.message).fontSize(14)
      TextInput({ placeholder: 'input your word', text: this.inputText })
        .height(60)
        .border({ width: 5, color: Color.Red })
        .placeholderColor(Color.Blue)
        .placeholderFont({ size: 20, weight: FontWeight.Normal, family: "sans-serif", style: FontStyle.Italic })
        .caretColor(Color.Blue)
        .enterKeyType(EnterKeyType.Search)
        .onChange((value: string) => {
          this.inputText = value
        })

      Button('生成二维码').fontSize(25).width(300).margin(20).onClick(() => this.encode())
      Button('解析二维码').fontSize(25).width(300).margin(20).onClick(() => this.decode())
    }

  }
}